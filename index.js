const express = require('express');
const app = express();
const mongoose = require('mongoose');
const homeRouter = require('./routes/homeRouter');

require('dotenv').config();

mongoose.connect(process.env.MONGO_URL);

const db = mongoose.connection;

db.on('error', () => console.log('Erro ao conectar ao MongoDB'));
db.once('open', () => console.log('Conectado ao MongoDB'));

app.use('/', homeRouter);

app.listen(process.env.PORT, () => console.log(`Run API Server in http://localhost:${process.env.PORT}`));