const mongoose = require('mongoose');

module.exports = mongoose.model('Developer', {
    nome: { type: String, required: true },
    cidade: { type: String, default: 'Não Informado' },
    senioridade: { type: String, default: 'Não Informado' },
});