const Developer = require('../models/Developer');

const index = async (req, res) => {
    try{
        let devs = await Developer.find({});
        res.send(devs);

    }catch(err){
        console.error(err);
    }
};

module.exports = { index }