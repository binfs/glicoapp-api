FROM node:latest

RUN mkdir -p /home/node/app
RUN chown -R node:node /home/node/app

WORKDIR /home/node/app

COPY . .

EXPOSE 3000

CMD ["node", "index"]